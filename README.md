# Arch Linux Setup

To run type the command below, after booted on arch iso:

```
curl -LO https://gitlab.com/cnkndmr/arch-linux-setup/-/raw/main/arch_install.sh
sh arch_install.sh
```

```sh
curl -LO https://gitlab.com/cnkndmr/arch-linux-setup/-/raw/main/post_install.sh
sh post_install.sh
```

Arch linux install script mostly inspired by https://github.com/deepbsd/Farchi
