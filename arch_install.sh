#!/bin/bash

# Local settings
TIME_ZONE="Europe/Istanbul"
LOCALE="en_US.UTF-8"

# System packages
BASE_SYSTEM=(base base-devel linux linux-headers linux-firmware)
EXTRA_SYSTEM=(mate mate-terminal xorg xorg-server lightdm lightdm-gtk-greeter)

# Checking boot mode
efi_boot_mode() {
    [[ -d /sys/firmware/efi/efivars ]] && return 0
    return 1
}

# Printing errors
error() { echo "Error: $1" && exit 1; }

# Formatting partitions
format_it() {
    device=$1
    fstype=$2
    mkfs."$fstype" "$device" || error "format_it(): Can't format device $device with $fstype"
}

# Mounting partitions
mount_it() {
    device=$1
    mt_pt=$2
    mount "$device" "$mt_pt" || error "mount_it(): Can't mount $device to $mt_pt"
}

# Checking reflector update
clear
echo && echo "Waiting until reflector has finished updating mirrorlist..."
while true; do
    pgrep -x reflector &>/dev/null || break
    echo -n '.'
    sleep 2
done

# Checking internet connection
echo && echo "Testing internet connection..."
$(ping -c 3 archlinux.org &>/dev/null) || (echo "Not Connected to Network!!!" && exit 1)
echo "Good!  We're connected!!!" && sleep 3

# Checking time date information
timedatectl set-ntp true
echo && echo "Date/Time service Status is . . . "
timedatectl status
sleep 4

clear
# Setting up swap size
if [ -z "$1" ]; then
    unset SWAP_SIZE
    until [[ $SWAP_SIZE ]]; do
        echo "Please enter the size of swap partition (eg: 12G, 512M):"
        read SWAP_SIZE
    done
fi

# Setting up host name
unset HOSTNAME
until [[ $HOSTNAME ]]; do
    echo "Please provide a hostname:"
    read HOSTNAME
done

# Setting up username
unset sudo_user
until [[ $sudo_user ]]; do
    echo "Please provide a username:"
    read sudo_user
done

# Setting up host password
unset HOST_PASS
until [[ $HOST_PASS ]]; do
    echo "Password for root:"
    read HOST_PASS
done

# Setting up username password
unset USER_PASS
until [[ $USER_PASS ]]; do
    echo "Password for $sudo_user:"
    read USER_PASS
done

echo "All settings stored. Screen cleared in 4 seconds..."
sleep 4
clear

# Partitioning drive
echo "Partitioning Drive"

IN_DEVICE=/dev/sda
FILESYSTEM=ext4

if [ -z "$1" ]; then
    if $(efi_boot_mode); then
        EFI_MTPT=/mnt/boot/efi
        EFI_DEVICE="${IN_DEVICE}1"
    else
        unset EFI_DEVICE
        BOOT_MTPT=/mnt/boot
        BOOT_DEVICE="${IN_DEVICE}1"
    fi

    SWAP_DEVICE="${IN_DEVICE}2"
    HOME_DEVICE="${IN_DEVICE}3"

    if $(efi_boot_mode); then
        EFI_SIZE=512M
        unset BOOT_SIZE
    else
        unset EFI_SIZE
        unset EFI_MTPT
        BOOT_SIZE=512M
    fi

    if $(efi_boot_mode); then
        sgdisk -Z "$IN_DEVICE"
        sgdisk -n 1::+"$EFI_SIZE" -t 1:ef00 -c 1:EFI "$IN_DEVICE"
        sgdisk -n 2::+"$SWAP_SIZE" -t 2:8200 -c 2:SWAP "$IN_DEVICE"
        sgdisk -n 3 -c 3:HOME "$IN_DEVICE"
        format_it "$HOME_DEVICE" "$FILESYSTEM"
        mount_it "$HOME_DEVICE" /mnt
        mkfs.fat -F32 "$EFI_DEVICE"
        mkdir /mnt/boot && mkdir /mnt/boot/efi
        mount_it "$EFI_DEVICE" "$EFI_MTPT"
        mkswap "$SWAP_DEVICE"
        swapon "$SWAP_DEVICE"
    else
        cat >/tmp/sfdisk.cmd <<EOF
$BOOT_DEVICE : start=2048, size=+$BOOT_SIZE, type=83, bootable
$SWAP_DEVICE : size=+$SWAP_SIZE, type=82
$HOME_DEVICE : type=83
EOF
        sfdisk "$IN_DEVICE" </tmp/sfdisk.cmd
        mkdir "$BOOT_MTPT"
        format_it "$BOOT_DEVICE" "$FILESYSTEM"
        mount_it "$BOOT_DEVICE" "$BOOT_MTPT"
        format_it "$HOME_DEVICE" "$FILESYSTEM"
        mount_it "$HOME_DEVICE" /mnt
        mkswap "$SWAP_DEVICE"
        swapon "$SWAP_DEVICE"
    fi
else
    if [ $(echo "$1" | grep -Eo "$IN_DEVICE[0-9]$") ]; then
        echo "Partition formatting..."
        format_it "$1" "$FILESYSTEM"
        mount_it "$1" /mnt
        echo "Partition formatting done. $1 mounted to /mnt"
    else
        error "$1 partition not accepted."
    fi
fi

# Installing base system packages
echo && echo "Base system packages installing..."
pacman -Sy --noconfirm archlinux-keyring
pacstrap /mnt "${BASE_SYSTEM[@]}"
echo && echo "Base system installed."

# Generating fstab
echo "Generating fstab..."
genfstab -U /mnt >>/mnt/etc/fstab
echo && echo "Here's the new /etc/fstab..."
cat /mnt/etc/fstab

# Setting up time zone
echo && echo "Setting timezone to $TIME_ZONE..."
arch-chroot /mnt ln -sf /usr/share/zoneinfo/"$TIME_ZONE" /etc/localtime
arch-chroot /mnt hwclock --systohc --utc
arch-chroot /mnt date
echo && echo "Here's the date info..."

# Setting up locale
echo && echo "Setting locale to $LOCALE ..."
arch-chroot /mnt sed -i "s/#$LOCALE/$LOCALE/g" /etc/locale.gen
arch-chroot /mnt locale-gen
echo "LANG=$LOCALE" >/mnt/etc/locale.conf
export LANG="$LOCALE"
cat /mnt/etc/locale.conf
echo && echo "Here's your /mnt/etc/locale.conf."

# Setting hostname
echo && echo "Setting hostname..."
sleep 3
echo "$HOSTNAME" >/mnt/etc/hostname
cat >/mnt/etc/hosts <<HOSTS
127.0.0.1      localhost
::1            localhost
127.0.1.1      $HOSTNAME.localdomain     $HOSTNAME
HOSTS

echo && echo "/etc/hostname and /etc/hosts files configured..."
echo "/etc/hostname . . ."
cat /mnt/etc/hostname
echo "/etc/hosts . . ."
cat /mnt/etc/hosts
echo && echo "Here are /etc/hostname and /etc/hosts."

# Setting up root password
echo "Setting ROOT password..."
echo "root:$HOST_PASS" | arch-chroot /mnt chpasswd

# Installing Network Manager
echo && echo "Enabling NetworkManager service..."
arch-chroot /mnt pacman -S --noconfirm networkmanager
arch-chroot /mnt systemctl enable NetworkManager.service

# Adding user account
echo && echo "Adding sudo + user acct..."
sleep 2
arch-chroot /mnt pacman -S --noconfirm sudo
arch-chroot /mnt sed -i 's/# %wheel/%wheel/g' /etc/sudoers
arch-chroot /mnt sed -i 's/%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers
echo && echo "Creating $sudo_user and adding $sudo_user to sudoers..."
arch-chroot /mnt useradd -m -G wheel "$sudo_user"
echo "$sudo_user:$USER_PASS" | arch-chroot /mnt chpasswd

# Installing desktop environement
echo "Installing X and X Extras and Video Driver."
arch-chroot /mnt pacman -S --noconfirm "${EXTRA_SYSTEM[@]}"
echo "Enabling display manager service..."
arch-chroot /mnt systemctl enable lightdm.service
echo && echo "Your desktop and display manager should now be installed..."
sleep 5

# Installing grub
echo "Installing grub..."
sleep 4
arch-chroot /mnt pacman -S --noconfirm grub os-prober

if $(efi_boot_mode); then
    arch-chroot /mnt pacman -S --noconfirm efibootmgr

    [[ ! -d /mnt/boot/efi ]] && error "Grub Install: no /mnt/boot/efi directory!!!"
    arch-chroot /mnt grub-install "$IN_DEVICE" --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi

    ## This next bit is for Ryzen systems with weird BIOS/EFI issues; --no-nvram and --removable might help
    [[ $? != 0 ]] && arch-chroot /mnt grub-install \
        "$IN_DEVICE" --target=x86_64-efi --bootloader-id=GRUB \
        --efi-directory=/boot/efi --no-nvram --removable
    echo "efi grub bootloader installed..."
else
    arch-chroot /mnt grub-install "$IN_DEVICE"
    echo "mbr bootloader installed..."
fi
echo "configuring /boot/grub/grub.cfg..."
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

echo "System should now be installed and ready to boot!!!"
echo && echo "Type 'shutdown -h now' and remove Installation Media and then reboot"
