#!/bin/bash

# Installing base packages.
echo "Installing base packages..."
sudo pacman -Syu --noconfirm archlinux-keyring ttf-ubuntu-font-family materia-gtk-theme i3-gaps mate-extra zsh git noto-fonts

# Cloning arch-linux-setup repo
echo "Downloading cnkndmr/arch-linux-setup.git"
git clone https://gitlab.com/cnkndmr/arch-linux-setup.git

# Setting up config files.
echo "Config files loading..."
cp -R arch-linux-setup/files/home/. ~/

# Restoring dconf setup.
echo "Dconf files loading..."
for file in arch-linux-setup/files/dconf/*
do
	dconf load / < "$file"
done

# Setting up antigen
echo "Downloading antigen..."
mkdir -p ~/.config/antigen
curl -L git.io/antigen > ~/.config/antigen/antigen.zsh

echo "Changing shell to zsh..."
chsh -s $(which zsh)

# Finishing installation.
echo "Basic installation completed. Press enter to logout:"
read
kill -9 -1
